#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <Servo.h>

#include "index.h"
#include "nipplejs.h"
#include "config.h"

#define LED 2
#define ServoPinR 14
#define ServoPinL 13
#define ServoPinC 12

int Lcorrect = 96;
int Rcorrect = 96;


Servo servoL;
Servo servoR;
Servo servoC;

ESP8266WebServer server(80);

void handleData() {
  if (server.arg("state") == "start") {servoL.attach(ServoPinL);servoR.attach(ServoPinR);servoR.write(96);servoL.write(96);server.send(200, "text/plane","");}
  if (server.arg("state") == "end") {servoL.detach();servoR.detach();server.send(200, "text/plane","");}
  if (server.argName(0) == "but") {
    String but = server.arg("but");
    int button = but.toInt();
    if (button == 1) {servoC.attach(ServoPinC);servoC.write(30);server.send(200, "text/plane","");}
    if (button == 2) {servoC.attach(ServoPinC);servoC.write(120);server.send(200, "text/plane","");}
      }
}

void handleServo() {
  server.send(200, "text/plane","");
  String Lservo = server.arg("LSer");
  String Rservo = server.arg("RSer");
  int LtoWrite = Lservo.toInt();
  int RtoWrite = Rservo.toInt();
  LtoWrite = Lcorrect + LtoWrite;
  RtoWrite = Rcorrect - RtoWrite;
  servoR.write(RtoWrite);servoL.write(LtoWrite);
}

void handleRoot() {
 server.send(200, "text/html", MAIN_page); //Send web page
}

void NippleJS() {
 server.send(200, "text/script", Nipple_JS); //Send web page
}

void setup() {
  delay(1000);

  pinMode(LED,OUTPUT);
  
  WiFi.softAP(ap_ssid, ap_pass);
  server.on("/",handleRoot);
  server.on("/nipplejs.js",NippleJS);
  server.on("/data",handleData);
  server.on("/servo",handleServo);
  server.begin();  
}

void loop() {
 server.handleClient();
}
