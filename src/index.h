const char MAIN_page[] PROGMEM = R"=====(
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <style>@charset "UTF-8";
        html,
        body {
            width: 100%;
            height: 100%;
            margin: 0;
            padding: 0;
            background-color: green;
        }
        
        #container {
            width: inherit;
            height: inherit;
            margin: 0;
            padding: 0;
            background-color: pink;
        }
        
        h1 {
            margin: 0;
            padding: 0;
        }
        
        html,
        body,
        div,
        h1,
        h2,
        h3,
        h4,
        h5,
        h6,
        p,
        ul,
        ol,
        li,
        a,
        span,
        blockquote,
        label,
        input,
        textarea,
        form,
        fieldset {
            border: 0;
            font: inherit;
            font-size: 100%;
            margin: 0;
            padding: 0;
            vertical-align: baseline;
        }
        
        .hidden {
            visibility: hidden;
        }
        
        .button_ {
            border: none;
            color: white;
            text-decoration: none;
            font-size: 30px;
            cursor: pointer;
            border-radius: 50%;
            width: 100px;
            height: 100px;
            position: absolute;
            display: flex;
            align-items: center;
            justify-content: center;
            z-index: 200;
        }
        
        #button_esc {
            border: none;
            color: white;
            text-decoration: none;
            font-size: 10px;
            cursor: pointer;
            border-radius: 20%;
            width: 60px;
            height: 30px;
            position: absolute;
            display: flex;
            align-items: center;
            justify-content: center;
            z-index: 200;
            background-color: #777;
            opacity: 0.3;
            left: 60px;
            top: 30px;
        }
        
        #button_esc:hover {
            opacity: 0.4;
        }
        
        #button_x {
            background-color: #04AA6D;
            opacity: 0.3;
            right: 3%;
            bottom: 4%;
        }
        
        #button_x:hover {
            opacity: 0.4;
        }
        
        #button_o {
            background-color: #AA046D;
            opacity: 0.3;
            right: 3%;
            bottom: 15%;
        }
        
        #button_o:hover {
            opacity: 0.4;
        }
        
        #game-box {
            width: 100%;
            position: relative;
        }
        
        #section {
            width: 100%;
            max-width: 970px;
            margin-left: auto;
            margin-right: auto;
            margin-top: 24px;
        }
        
        #section:after {
            content: ".";
            display: block;
            height: 0;
            clear: both;
            visibility: hidden;
        }
        
        .hidden {
            visibility: hidden;
        }
        
        .noselect {
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        
        div.button {
            color: white !important;
            background-color: #FF6651;
            text-decoration: none;
            font-weight: bold;
            -webkit-border-radius: 18px;
            -moz-border-radius: 18px;
            border-radius: 18px;
            box-shadow: 2px 2px 2px #aaa;
            cursor: pointer;
            width: fit-content;
            display: inline-block;
            padding: 2px 0px;
            margin: 2px 12px;
            -webkit-transition: all 0.2s ease;
            -moz-transition: all 0.2s ease;
            -o-transition: all 0.2s ease;
            -ms-transition: all 0.2s ease;
            transition: all 0.2s ease;
        }
        
        div.button:hover {
            background-color: #F2462E;
            color: white !important;
            text-decoration: none !important;
            font-weight: bolder;
            padding: 4px 2px;
            margin: 0px 10px;
            -webkit-transition: all 0.2s ease;
            -moz-transition: all 0.2s ease;
            -o-transition: all 0.2s ease;
            -ms-transition: all 0.2s ease;
            transition: all 0.2s ease;
        }
        
        div.button a {
            display: block;
            height: fit-content;
            width: fit-content;
            text-decoration: none;
            color: white !important;
            padding: 5px 20px 5px 20px;
            text-decoration: none;
            font-weight: bold;
        }
        
        div.button:hover a {
            color: white !important;
            text-decoration: none !important;
        }
        
        p.clear {
            clear: both;
        }
        
        .clear {
            clear: both;
        }</style>
    <title>Tractor</title>
    <meta http-equiv="content-type" content="text/html" />
    <meta name="robots" content="noindex, nofollow" />
</head>

<body>
<div class="static-box" style="height: 100%; width: 100%;">
    <div class="noselect" id="box" style="height: 100%; width: 100%; touch-action: none;"></div>

    <script src="nipplejs.js"></script>
    <script>
        var saveL = 0;
        var saveR = 0;

        function sendData(toWriteL, toWriteR) {
            var snd = new XMLHttpRequest();
            snd.open("GET", "servo?LSer=" + toWriteL + "&RSer=" + toWriteR, true);
            snd.send();
        }

        function sendState(state) {
            var snd = new XMLHttpRequest();
            snd.open("GET", "data?state=" + state, true);
            snd.send();
        }

        function sendbutton(but) {
            var snd = new XMLHttpRequest();
            snd.open("GET", "data?but=" + but, true);
            snd.send();
        }

        function createJoystick() {
            var nippleManager = nipplejs.create({
                zone: document.getElementById('box'),
                color: 'red',
                mode: 'static',
                catchDistance: 700,
                position: { left: '50%', bottom: '30%' },
                size: 700,

            });

            var nipple = nippleManager;
            nipple.on('start', function (evt, data) {
                sendState("start")
            }).on('move', function (evt, data) {

                if (data.distance > 20) {
                    var deg = data.angle.degree;
                    var speed = Math.round(data.distance / 2);

                    if (deg >= 0 && deg < 45) {
                        var currentDeg = 45 - deg;
                        Rout = -Math.round(currentDeg / 5);
                        Lout = 9;
                    }
                    else if (deg >= 45 && deg < 90) {
                        var currentDeg = deg - 45;
                        Rout = Math.round(currentDeg / 5);
                        Lout = 9;
                    }
                    else if (deg >= 90 && deg < 135) {
                        var currentDeg = 135 - deg;
                        Rout = 9;
                        Lout = Math.round(currentDeg / 5);
                    }
                    else if (deg >= 135 && deg < 180) {
                        var currentDeg = deg - 135;
                        Rout = 9;
                        Lout = -Math.round(currentDeg / 5);
                    }
                    if (deg >= 180 && deg < 225) {
                        var currentDeg = 225 - deg;
                        Rout = Math.round(currentDeg / 5);
                        Lout = -9;
                    }
                    else if (deg >= 225 && deg < 270) {
                        var currentDeg = deg - 225;
                        Rout = -Math.round(currentDeg / 5);
                        Lout = -9;
                    }
                    else if (deg >= 270 && deg < 315) {
                        var currentDeg = 315 - deg;
                        Rout = -9;
                        Lout = -Math.round(currentDeg / 5);
                    }
                    else if (deg >= 315 && deg < 360) {
                        var currentDeg = deg - 315;
                        Rout = -9;
                        Lout = Math.round(currentDeg / 5);
                    }
                    speedC = Math.round(data.distance / 5 / 3) / 10;
                    toWriteL = Math.round(Lout * speedC)
                    toWriteR = Math.round(Rout * speedC)
                    if (toWriteL != saveL || toWriteR != saveR) {
                        saveL = toWriteL;
                        saveR = toWriteR;
                        sendData(toWriteL, toWriteR)
                    }
                }


            }).on('end', function (evt) {
                sendState("end")
            });
            var buttonX = document.createElement("a");
            var buttonXText = document.createElement("span");
            buttonXText.id = "button_x";
            buttonXText.innerText = "DOWN";
            buttonX.appendChild(buttonXText);
            buttonX.onpointerdown = pressButtonX;
            document.getElementById('box').appendChild(buttonX);

            var buttonO = document.createElement("a");
            var buttonOText = document.createElement("span");
            buttonOText.id = "button_o";
            buttonOText.innerText = "UP";
            buttonO.appendChild(buttonOText);
            buttonO.onpointerdown = pressButtonO;
            document.getElementById('box').appendChild(buttonO);
        }

        function pressButtonX() {sendbutton(1)}

        function pressButtonO() {sendbutton(2)}

        createJoystick();
    </script>
    <style>
        #button_x {
            background-color: #04AA6D;
            border: none;
            color: white;
            text-decoration: none;
            font-size: 100px;
            cursor: pointer;
            border-radius: 50%;
            width: 400px;
            height: 400px;
            position: absolute;
            left: 5%;
            top: 5%;
            display: flex;
            align-items: center;
            justify-content: center;
            z-index: 200;
        }

        #button_o {
            background-color: #AA046D;
            border: none;
            color: white;
            text-decoration: none;
            font-size: 100px;
            cursor: pointer;
            border-radius: 50%;
            width: 400px;
            height: 400px;
            position: absolute;
            right: 5%;
            top: 5%;
            display: flex;
            align-items: center;
            justify-content: center;
            z-index: 201;
        }

        #box {
            width: 100%;
            min-width: 560px;
            min-height: 410px;
            background-color: #bbcccc;
            position: relative;
        }

        .noselect {
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
    </style>

    </body>

</html>
)=====";
